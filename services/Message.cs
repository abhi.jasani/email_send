using System.Linq;
using MailKit.Net.Smtp;
using MimeKit;

public class Message
{
    public List<MailboxAddress> To { get; set; }
    public string Subject { get; set; }
    public string Content { get; set; }
    public IFormFileCollection Attachments { get; set; }

    public Message(string to, string subject, string content, IFormFileCollection file)
    {
        To = new List<MailboxAddress>();

        To.AddRange(to.Select(x => new MailboxAddress(to, to)));
        Subject = subject;
        Content = content;
        Attachments = file;
    }
}


public interface IEmailSender
{
    Task SendEmailAsync(Message message);
}