using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using EmailServices.EmailConfiguration;
using MimeKit;

namespace EmailServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {
        private readonly IEmailSender _emailSender;
        private readonly IConfiguration _configuration;

        public EmailController(IEmailSender emailSender, IConfiguration configuration)
        {
            _emailSender = emailSender;
            _configuration = configuration;
        }

        [HttpPost("send")]
        public async Task<IActionResult> SendEmail([FromForm] EmailRequest emailRequest)
        {

            var files = Request.Form.Files.Any() ? Request.Form.Files : new FormFileCollection();
            // Create message
            var message = new Message(
                emailRequest.To,
                emailRequest.Subject,
                emailRequest.Content,
                emailRequest.Attachments
            );
            Console.WriteLine(emailRequest.To);
            // Inside your SendEmail method


            try
            {
                // Send email
                await _emailSender.SendEmailAsync(message);
                return Ok("Email sent successfully!");
            }
            catch (Exception ex)
            {
                // Handle error
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }
    }

    public class EmailRequest
    {
        public string To { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }

        public IFormFileCollection Attachments { get; set; }
    }
}
